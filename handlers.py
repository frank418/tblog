#-*- coding:UTF-8 -*-
__author__ = 'Frank Yang <frankyang418@gmail.com>'
__date__ = "13-5-4 上午9:08"
__doc__ = u"请求处理器"

import tornado.web
import tornado.escape
from forms import LoginForm
from mixin import FlashMessageMixin
from vendor.Session import Session


class BaseHandler(tornado.web.RequestHandler, FlashMessageMixin):
    def get_current_user(self):
        c = 'CURRENT_USER'
        if c in self.session:
            return self.session[c]
        else:
            return None

    @property
    def session(self):
        if not hasattr(self, '_session'):
            self._session = Session(self.get_secure_cookie, self.set_secure_cookie)
        return self._session

    def write_error(self, status_code, **kwargs):
        pass


class HomeIndexHandler(BaseHandler):
    def get(self):
        self.render('home/index.html')


class AccountLoginHandler(BaseHandler):
    def get(self):
        form = LoginForm(self)
        self.render('account/login.html', form=form)

    def post(self):
        form = LoginForm(self)
        if form.validate():
            pass
        else:
            for field in form.errors:
                self.flash(form.errors[field][0], category='error')
            self.redirect(self.reverse_url('AccountLogin'))


class AccountLogoutHandler(BaseHandler):
    def get(self):
        self.session.clear()