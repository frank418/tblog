# -*- coding=UTF-8 -*-
__author__ = 'Frank Yang <frankyang418@gmail.com>'
__date__ = '13-5-5 下午2:06'
__doc__ = 'For Bae Environment'
from helpers import setting_from_object
import setting
import tornado.wsgi
from urls import urls
import ui
from bae.api import logging

settings = setting_from_object(setting)
settings['ui_modules'] = ui
app = tornado.wsgi.WSGIApplication(urls, **settings)
from bae.core.wsgi import WSGIApplication
application = WSGIApplication(app)
