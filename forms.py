#-*- coding:UTF-8 -*-
__author__ = 'Frank Yang <frankyang418@gmail.com>'
__date__ = "13-5-4 上午9:06"
__doc__ = u"表单模型"

from vendor.wtforms import Form as BaseForm
from vendor.MultiValueDict import MultiValueDict
from vendor.wtforms import TextField, PasswordField
from vendor.wtforms.validators import Required


class Form(BaseForm):
    def __init__(self, handler=None, obj=None, prefix='', formdata=None, **kwargs):
        if handler:
            formdata = MultiValueDict()
            for name in handler.request.arguments.keys():
                formdata.setlist(name, handler.get_arguments(name))
        BaseForm.__init__(self, formdata, obj=obj, prefix=prefix, **kwargs)


class LoginForm(Form):
    uid = TextField(label=u"用户名", validators=[Required(u"用户名必须填写")])
    pwd = PasswordField(label=u"密码", validators=[Required(u"密码必须填写")])