#-*- coding:UTF-8 -*-
__author__ = 'Frank Yang <frankyang418@gmail.com>'
__date__ = "13-4-24 上午10:12"
__doc__ = u"ui modules"
import tornado.web


class Form(tornado.web.UIModule):
    def render(self, form):
        return self.render_string('ui/form.html', form=form)