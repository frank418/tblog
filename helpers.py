#-*- coding:UTF-8 -*-
__author__ = 'Frank Yang <frankyang418@gmail.com>'
__date__ = "13-5-4 上午9:12"
__doc__ = u""


def setting_from_object(obj):
    """
    从一个对象中读取配置
    :param obj:配置对象
    :return: dict
    """
    setting = dict()
    for key in dir(obj):
        setting[key.lower()] = getattr(obj, key)
    return setting