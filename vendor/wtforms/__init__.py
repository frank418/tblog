"""
WTForms
=======

WTForms is a flexible forms validation and rendering library for python web
development.

:copyright: Copyright (c) 2010 by Thomas Johansson, James Crasta and others.
:license: BSD, see LICENSE.txt for details.
"""
from vendor.wtforms import validators, widgets
from vendor.wtforms.fields import *
from vendor.wtforms.form import Form
from vendor.wtforms.validators import ValidationError

__version__ = '1.0.3'
