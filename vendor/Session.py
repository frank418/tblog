#-*- coding:UTF-8 -*-
__author__ = 'Franke Yang <frankyang418@gmail.com>'
__date__ = "13-5-4 上午9:19"
__doc__ = u"会话类"

try:
    import cPickle as pickle
except ImportError:
    import pickle


class Session(object):
    def __init__(self, get_secure_cookie, set_secure_cookie, name='_session', expires_days=None):
        self.set_session = set_secure_cookie
        self.get_session = get_secure_cookie
        self.name = name
        self._expiry = expires_days
        self._dirty = False
        self.get_data()

    def get_data(self):
        value = self.get_session(self.name)
        self._data = pickle.loads(value) if value else {}

    def set_expires(self, days):
        self._expiry = days

    def __getitem__(self, key):
        return self._data[key]

    def __setitem__(self, key, value):
        self._data[key] = value
        self._dirty = True
        self.save()

    def __delitem__(self, key):
        if key in self._data:
            del self._data[key]
            self._dirty = True
            self.save()

    def __getattr__(self, key):
        if key in self._data:
            return self._data[key]
        else:
            return None

    def __contains__(self, key):
        return key in self._data

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        for key in self._data:
            yield key

    def __del__(self):
        self.save()

    def save(self):
        if self._dirty:
            self.set_session(self.name, pickle.dumps(self._data), expires_days=self._expiry)
            self._dirty = False

    def clear(self):
        self.set_session(self.name,None)