#-*- coding:UTF-8 -*-
import tornado
import tornado.web
from tornado.options import options, define
from helpers import setting_from_object
import setting
import ui

define('cmd', default='run', metavar="run|syncdb|test", help=u"命令s")
define('port', default=4000, type=int, help=u"监听端口")
define('host', default='127.0.0.1', type=str, help=u"监听地址")


class Application(tornado.web.Application):
    def __init__(self):
        from urls import urls

        handlers = urls
        settings = setting_from_object(setting)
        settings['ui_modules'] = ui
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == '__main__':
    options.parse_command_line()
    if options.cmd == 'run':
        import tornado.httpserver
        import tornado.ioloop

        http_server = tornado.httpserver.HTTPServer(Application(), xheaders=True)
        http_server.listen(options.port, address=options.host)
        print "Server is running - http://%s:%d" % (options.host, options.port)
        tornado.ioloop.IOLoop.instance().start()
    elif options.cmd == 'syncdb':
        pass
    elif options.cmd == 'test':
        pass
    else:
        options.print_help()