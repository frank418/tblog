#-*- coding:UTF-8 -*-
__author__ = 'Frank Yang <frankyang418@gmail.com>'
__date__ = "13-4-24 上午8:45"
__doc__ = u"配置文件"

import os

DEBUG = True
LOGIN_URL = '/account/login'
COOKIE_SECRET = 'MTgwODczOTgzMjA='
STATIC_PATH = os.path.join(os.path.dirname(__file__), 'static')
TEMPLATE_PATH = os.path.join(os.path.dirname(__file__), 'templates')
XSRF_COOKIES = True
GZIP = True