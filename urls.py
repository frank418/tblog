#-*- coding:UTF-8 -*-
__author__ = 'Frank Yang <frankyang418@gmail.com>'
__date__ = "13-4-24 上午9:29"
__doc__ = u"路由"

import tornado.web
import handlers

urls = [
    tornado.web.URLSpec('/', handlers.HomeIndexHandler, name='HomeIndex'),
    tornado.web.URLSpec('/x/login', handlers.AccountLoginHandler, name='AccountLogin'),
    tornado.web.URLSpec('/x/logout', handlers.AccountLogoutHandler, name='AccountLogout')
]